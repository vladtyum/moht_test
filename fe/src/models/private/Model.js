import { toCamelCase } from '../../services/helpers'
import moment from 'moment'

export default class Model {
    constructor(obj) {
        for (let key in obj) {
            let value = obj[key]

            let model = Model

            if (Array.isArray(value) && value.length === 0) {
                value = []
            } else if (Array.isArray(value) && value.length > 0) {
                let firstItem = value[0]
                if (firstItem instanceof Object) {
                    // Recursion
                    for (let i = 0; i < value.length; i++) {
                        value[i] = new model(value[i])
                    }
                }
            } else if (value instanceof Object) {
                value = new model(value)
            }

            this.set(toCamelCase(key), value)
        }
    }

    set(key, value) {
        if (value === '0001-01-01T00:00:00Z') {
            value = null
        }

        if (this.isDate(value)) {
            value = moment.utc(value, 'YYYY-MM-DDTHH:mm:ss')
        }

        this[key] = value
    }

    isDate(value) {
        let ptn1 = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/
        let ptn2 = /\d{4}-\d{2}-\d{2}/

        return ptn1.test(value) || ptn2.test(value)
    }
}
