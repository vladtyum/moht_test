import Model from './private/Model'

export default class Transaction extends Model {
    get transactionText() {
        let s = `Transferred $${this.absoluteAmount} ${this.type === 'deposit' ? 'to' : 'from'}`
        s += ` account ${this.accountId}`
        return s
    }

    get balanceText() {
        return this.balance === null ? '' : `The current account balance is $${this.balance}`
    }

    get absoluteAmount() {
        return Math.abs(this.amount)
    }
}
