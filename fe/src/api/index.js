import { logMessage } from '../services/helpers'
import { API_URL } from '../services/constants'

export const TRANSACTIONS_URL = '/transactions'

/**
 * Generic api call to all endpoints.
 * Under any circumstance function should
 * return result object {isError, data}.
 *
 * @param {string}  url
 * @param {string}  method - GET, POST, PUT, PATCH, DELETE
 * @param {Object}  data
 * @param {boolean} isAuth - Does request need token.
 *
 * @returns {Object}  obj
 * @returns {boolean} obj.isError
 * @returns {Object}  obj.data - requested data from server or errors object
 *
 * data errors example: possible keys {
 *   field1: ['error1'],
 *   field2: ['error2', 'error3'],
 *   errors: ['error4']}
 *
 */
export async function apiCall(url, method, data, isAuth) {
    if (!url.startsWith('http')) {
        url = API_URL + url
    }

    isAuth = isAuth || false

    let response
    let result = { isError: false }

    let token = ''
    if (isAuth) {
        token = window.localStorage.getItem('token')
        if (token === null) {
            result.isError = true
            result.data = { errors: ['The token is not provided.'] }
            logMessage(method, data, url, result)
            return result
        }
    }

    try {
        let fetchObj = { method: method }
        fetchObj.headers = {}

        if (method === 'POST' || method === 'PUT' || method === 'PATCH') {
            fetchObj.body = JSON.stringify(data)
            fetchObj.headers = {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        }

        if (isAuth) {
            fetchObj.headers.Authorization = 'Token ' + token
        }

        response = await fetch(url, fetchObj)
    } catch (e) {
        result.isError = true
        result.data = { errors: ['Server error.'] }
        logMessage(method, data, url, result)
        return result
    }

    const status = response.status

    if (status === 404) {
        result.isError = true
        result.data = { errors: ['Resource not found.'] }
        logMessage(method, data, url, result)
        return result
    }

    if (status === 500) {
        result.isError = true
        result.data = { errors: ['Internal server error.'] }
        logMessage(method, data, url, result)
        return result
    }

    if (status === 204) {
        result.data = {}
        logMessage(method, data, url, result)
        return result
    }

    let obj = await response.json()

    if (status >= 200 && status <= 300) {
        result.data = obj
        logMessage(method, data, url, result)
        return result
    }

    // In all other cases should be valid json object with errors.

    let erObj = {}

    if (obj.hasOwnProperty('detail')) {
        erObj.errors = [obj.detail]
        delete obj.detail
    } else if (obj.hasOwnProperty('errors')) {
        erObj.errors = [obj.non_field_errors]
        delete obj.errors
    }

    // Other fields
    for (let key in obj) {
        erObj[key] = obj[key]
    }

    result.isError = true
    result.data = erObj
    logMessage(method, data, url, result)
    return result
}
