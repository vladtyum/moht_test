import './App.css'
import React from 'react'

import { TRANSACTIONS_URL, apiCall } from './api'
import Transaction from './models/Transaction'

import TransactionForm from './components/TransactionForm'
import Transactions from './components/Transactions'

import { AllContext } from './services/constants'

class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            contextValue: {
                createTransaction: this.createTransaction,
                fetchTransactions: this.fetchTransactions,
                transactions: [],
                isInProgress: false
            }
        }
    }

    updateProgress = (value) => {
        this.setState({
            contextValue: {
                ...this.state.contextValue,
                isInProgress: value
            }
        })
    }

    updateTransactions = (value) => {
        this.setState({
            contextValue: {
                ...this.state.contextValue,
                transactions: value
            }
        })
    }

    // Returns created transaction or null
    createTransaction = async (accountId, amount) => {
        this.updateProgress(true)

        let form = {
            account_id: accountId,
            amount: amount
        }

        let url = TRANSACTIONS_URL

        let result = await apiCall(url, 'POST', form, false)

        this.updateProgress(false)
        return result.isError ? null : new Transaction(result.data)
    }

    fetchTransactions = async (accountId) => {
        this.updateProgress(true)

        let url = TRANSACTIONS_URL

        if (accountId != null) {
            url += `?account_id=${accountId}`
        }

        console.log(accountId)
        console.log(url)

        let arr = []
        let result = await apiCall(url, 'GET', {}, false)

        if (!result.isError) {
            for (let d of result.data) {
                arr.push(new Transaction(d))
            }
        }

        this.updateProgress(false)
        this.updateTransactions(arr)
    }

    render() {
        return (
            <AllContext.Provider value={this.state.contextValue}>
                <section className="section">
                    <div className="container">
                        <div className="columns">
                            <div className="column is-4">
                                <p className="subtitle">Submit new transaction</p>
                                <TransactionForm />
                            </div>

                            <div className="column is-8">
                                <p className="subtitle">Historical transactions</p>
                                <Transactions />
                            </div>
                        </div>
                    </div>
                </section>
            </AllContext.Provider>
        )
    }
}

export default App
