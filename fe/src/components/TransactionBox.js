import React, { Component } from 'react'
import { PropTypes } from 'prop-types'

class TransactionBox extends Component {
    render() {
        const { transaction } = this.props

        return (
            <div className="container">
                <div
                    className="card"
                    data-type="transaction"
                    data-account-id={`${transaction.accountId}`}
                    data-amount={`${transaction.amount}`}
                    data-balance={`${transaction.balance}`}>
                    <div className="card-content">
                        <div>{transaction.transactionText}</div>
                        <div>{transaction.balanceText}</div>
                    </div>
                </div>
                <br />
            </div>
        )
    }
}

export default TransactionBox

TransactionBox.propTypes = {
    transaction: PropTypes.object
}
