import React, { Component } from 'react'
import { AllContext } from '../services/constants'
import { logMessage } from '../services/helpers'

import TransactionBox from './TransactionBox'

class Transactions extends Component {
    static contextType = AllContext

    async componentDidMount() {
        const { fetchTransactions } = this.context

        try {
            await fetchTransactions(null)
        } catch (error) {
            logMessage(error)
        }
    }

    render() {
        const { transactions } = this.context

        return (
            <div>
                {transactions.map((obj) => {
                    return <TransactionBox key={obj.id} transaction={obj} />
                })}
            </div>
        )
    }
}

export default Transactions
