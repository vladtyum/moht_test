import React, { Component } from 'react'
import { validateAmount, validateAccount } from '../services/helpers'
import { AllContext } from '../services/constants'

class TransactionForm extends Component {
    static contextType = AllContext

    constructor(props) {
        super(props)

        this.state = {
            accountId: '',
            amount: '',
            accountIdError: null,
            amountError: null
        }
    }

    get isButtonDisabled() {
        if (this.state.accountId.trim().length === 0) return true
        if (this.state.amount.trim().length === 0) return true
        if (!validateAmount(this.state.amount)) return true
        if (!validateAccount(this.state.accountId)) return true

        const { isInProgress } = this.context
        if (isInProgress) return true

        return false
    }

    onChangeAccountId = (e) => {
        let value = e.target.value
        this.setState({ accountId: value })

        if (!validateAccount(value)) {
            this.setState({ accountIdError: 'Incorrect account' })
        } else {
            this.setState({ accountIdError: null })
        }
    }

    onChangeAmount = (e) => {
        let value = e.target.value
        this.setState({ amount: value })

        if (!validateAmount(value)) {
            this.setState({ amountError: 'Incorrect value' })
        } else {
            this.setState({ amountError: null })
        }
    }

    onSubmit = async (e) => {
        e.preventDefault()

        const { createTransaction, fetchTransactions } = this.context
        let transaction
        if (validateAmount(this.state.amount) && validateAccount(this.state.accountId)) {
            transaction = await createTransaction(this.state.accountId, this.state.amount)
        };

        if (transaction !== null) {
            await fetchTransactions(transaction.accountId)
            this.setState({
                accountId: '',
                amount: '',
                amountError: null
            })
        }
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <div className="field">
                    <label className="label">Account ID</label>
                    <div className="control">
                        <input
                            className="input"
                            data-type="account-id"
                            onChange={this.onChangeAccountId}
                            type="text"
                            placeholder="Account ID"
                            value={this.state.accountId}
                        />
                    </div>
                    {this.state.accountIdError != null && (
                        <p className="help is-danger">Incorrect account</p>
                    )}
                </div>

                <div className="field">
                    <label className="label">Amount</label>
                    <div className="control">
                        <input
                            className="input"
                            data-type="amount"
                            onChange={this.onChangeAmount}
                            type="text"
                            placeholder="Amount"
                            value={this.state.amount}
                        />
                    </div>
                    {this.state.amountError != null && (
                        <p className="help is-danger">Incorrect amount</p>
                    )}
                </div>

                <div className="field">
                    <div className="control">
                        <button
                            // disabling for Asessment's Cypress Test 
                            // disabled={this.isButtonDisabled ? true : false}
                            data-type="transaction-submit"
                            className="button is-link">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        )
    }
}

export default TransactionForm
