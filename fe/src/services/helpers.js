export function validateAccount(acc){
    return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(acc);
}

// Integer or decimal
export function validateAmount(value) {
    return /^(-)?\d+\.\d{0,2}$/.test(value) || /^(-)?\d+$/.test(value)
}

export function toCamelCase(str) {
    return str.toLowerCase().replace(/(_|-)([a-z])/g, (str) => str[1].toUpperCase())
}

export function logMessage(...args) {
    console.log('_____________________')
    for (let message of args) {
        console.log(message)
    }
}

export function cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj))
}
