from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def create_superuser(self):
        User.objects.all().delete()
        User.objects.create_superuser("admin", "admin@a.com", "111")
        print("Superuser has been created")

    def handle(self, *args, **options):
        self.create_superuser()
