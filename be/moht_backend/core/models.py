from django.apps import apps
from django.contrib import admin
from django.db.models import (
    CASCADE,
    CharField,
    DateTimeField,
    DecimalField,
    ForeignKey,
    Model,
    Sum,
    TextChoices,
)

from core.services.helpers import get_guid


class Account(Model):
    account_id = CharField(max_length=50, default=get_guid, unique=True)

    def __str__(self):
        return f"Account: {self.account_id}"

    def get_balance(self, dt):
        """
        Get balance for all tranactions until dt
        """
        Transaction = apps.get_model("core", "Transaction")
        res = Transaction.objects.filter(
            account_id=self.id,
            created_at__lte=dt,
        ).aggregate(Sum("amount"))
        return res["amount__sum"]


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("account_id",)


class Transaction(Model):
    class TrType(TextChoices):
        DEPOSIT = "deposit", "DEPOSIT"
        WITHDRAWING = "withdrawing", "WITHDRAWING"

    account = ForeignKey(Account, on_delete=CASCADE)
    transaction_id = CharField(max_length=50, default=get_guid, unique=True)
    type = CharField(
        max_length=11,
        choices=TrType.choices,
        null=True,
        blank=True,
        editable=False,
    )
    amount = DecimalField(
        null=False, blank=False, max_digits=8, decimal_places=2
    )
    created_at = DateTimeField(auto_now_add=True, null=False, blank=False)

    class Meta:
        app_label = "core"
        verbose_name = "transaction"
        verbose_name_plural = "transactions"
        ordering = ["-created_at"]

    def __str__(self):
        return f"Transaction: {self.account} {self.amount} {self.created_at}"

    def save(self, *args, **kwargs):
        if self.amount >= 0:
            self.type = self.TrType.DEPOSIT
        elif self.amount < 0:
            self.type = self.TrType.WITHDRAWING
        super(Transaction, self).save(*args, **kwargs)


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        "account",
        "type",
        "transaction_id",
        "amount",
        "created_at",
    )
