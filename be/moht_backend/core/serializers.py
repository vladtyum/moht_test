from django.utils import timezone

from core.models import Account, Transaction
from rest_framework.serializers import (
    CharField,
    DecimalField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
)


class AccountSerializer(ModelSerializer):
    balance = SerializerMethodField()

    class Meta:
        model = Account
        fields = (
            "id",
            "account_id",
            "balance",
        )

    def get_balance(self, obj):
        return obj.get_balance(timezone.now())


class TransactionSerializer(ModelSerializer):
    account_id = CharField(source="account.account_id")
    balance = SerializerMethodField()
    amount = SerializerMethodField()

    class Meta:
        model = Transaction
        fields = (
            "id",
            "type",
            "account_id",
            "transaction_id",
            "amount",
            "created_at",
            "balance",
        )

    def get_amount(self, obj):
        # werid assessment's clause in cypress test 
        if obj.amount % 1 == 0:
            return int(obj.amount)
        return obj.amount
    
    def get_balance(self, obj):
        account_id = self.context.get("account_id", None)
        # if obj.amount >= 0:
        #     return None
        return f'{obj.account.get_balance(obj.created_at):g}'
        


class CreateTransactionSerializer(Serializer):
    account_id = CharField()
    amount = DecimalField(max_digits=8, decimal_places=2)

    def save(self):
        d = self.validated_data
        account, _ = Account.objects.get_or_create(account_id=d["account_id"])

        transaction = Transaction.objects.create(
            account=account, amount=d["amount"]
        )
        return transaction
