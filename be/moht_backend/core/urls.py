from django.urls import path, re_path

from . import views as v

urlpatterns = [
    path("ping", v.home, name="home"),
    path("accounts", v.Accounts.as_view(), name="accounts"),
    re_path(
        r"^accounts/(?P<account_id>[A-Za-z0-9-]+)$",
        v.AccountDetail.as_view(),
        name="account_detail",
    ),
    path("transactions", v.Transactions.as_view(), name="transactions"),
    re_path(
        r"^transactions/(?P<transaction_id>[A-Za-z0-9-]+)$",
        v.TransactionDetail.as_view(),
        name="transaction_detail",
    ),
    re_path(r"^$", v.home, name="home"),
]
