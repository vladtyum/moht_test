import uuid


def get_guid():
    return str(uuid.uuid4())
