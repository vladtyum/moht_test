from django.http import HttpResponse

from core.models import Account, Transaction
from core.serializers import (
    AccountSerializer,
    CreateTransactionSerializer,
    TransactionSerializer,
)
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from rest_framework.response import Response


def home(request):
    return HttpResponse("API")


class Accounts(ListCreateAPIView):
    """
    get:
        <h1>Get accounts list</h1>
        <b>Response:</b> List of account object<br />
        <b>Response code:</b> 200<br />
    """

    queryset = Account.objects.all()
    serializer_class = AccountSerializer


class AccountDetail(RetrieveAPIView):
    """
    get:
        <h1>Get account</h1>
        <b>Response:</b> Account object<br />
        <b>Response code:</b> 200<br />
    """

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    lookup_field = "account_id"


class Transactions(ListCreateAPIView):
    """
    get:
        <h1>Get transactions list</h1>
        <b>Query params:</b> transaction_id<br />
        <b>Response:</b> List of transaction object<br />
        <b>Response code:</b> 200<br />
    post:
        <h1>Create transaction</h1>
        <b>Input params:</b><br />
        account_id: string<br />
        amount: integer or decimal<br />
        <b>Response:</b> Created transaction object<br />
        <b>Response code:</b> 201<br />
    """

    queryset = Transaction.objects.all()

    def get_serializer_class(self):
        if self.request.method == "POST":
            return CreateTransactionSerializer
        return TransactionSerializer

    def get_serializer_context(self):
        account_id = self.request.query_params.get("account_id", None)
        return {"account_id": account_id}

    def post(self, request, **kwargs):
        s = CreateTransactionSerializer(
            data=request.data, context=self.get_serializer_context()
        )
        s.is_valid(raise_exception=True)
        transaction = s.save()
        s = TransactionSerializer(transaction)
        return Response(s.data, status=201)


class TransactionDetail(RetrieveAPIView):
    """
    get:
        <h1>Get transaction</h1>
        <b>Response:</b> Transaction object<br />
        <b>Response code:</b> 200<br />
    """

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    lookup_field = "transaction_id"
