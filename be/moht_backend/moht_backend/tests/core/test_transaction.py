from django.urls import reverse

import pytest

import json
from core.services.helpers import get_guid
from .factories import AccountFactory, TransactionFactory


class TestTransaction:
    @pytest.mark.django_db
    def test_create_transaction01(self, client):
        """
        Test to create transaction for not existing account.
        """
        url = reverse("transactions")

        d = {
            "account_id": get_guid(),
            "amount": 10,
        }
        data = json.dumps(d)

        r = client.post(url, data, content_type="application/json")
        assert r.status_code == 201

    @pytest.mark.django_db
    def test_create_transaction02(self, client):
        """
        Test to create transaction for existing account.
        """
        url = reverse("transactions")

        account = AccountFactory.get()

        d = {
            "account_id": account.account_id,
            "amount": 10,
        }
        data = json.dumps(d)

        r = client.post(url, data, content_type="application/json")
        assert r.status_code == 201
        assert r.json()["account_id"] == account.account_id

    @pytest.mark.django_db
    def test_get_transactions(self, client):
        """
        Test to get transactions without query params.
        """
        url = reverse("transactions")

        TransactionFactory.get()
        TransactionFactory.get()
        TransactionFactory.get()

        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3

    @pytest.mark.django_db
    def test_get_transactions_with_balance(self, client):
        """
        Test to get transactions with balance.
        """
        url = reverse("transactions")

        account = AccountFactory.get()
        url += f"?account_id={account.account_id}"

        TransactionFactory.get(account=account, amount=10)
        TransactionFactory.get(account=account, amount=-2)
        TransactionFactory.get(amount=100)

        r = client.get(url)
        assert r.status_code == 200

        transactions = r.json()
        assert len(transactions) == 3

        assert transactions[0]["balance"] is None
        assert transactions[1]["balance"] == 8
        assert transactions[2]["balance"] == 10

    @pytest.mark.django_db
    def test_get_transaction_detail(self, client):
        """
        Test to get transaction detail.
        """
        transaction = TransactionFactory.get()

        d = {"transaction_id": transaction.transaction_id}
        url = reverse("transaction_detail", kwargs=d)

        r = client.get(url)
        assert r.status_code == 200
