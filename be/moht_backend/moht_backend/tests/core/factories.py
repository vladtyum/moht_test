from django.utils import timezone

import factory

from core.models import Account, Transaction


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    @staticmethod
    def get():
        return AccountFactory()


class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Transaction

    created_at = timezone.now()

    @staticmethod
    def get(account=None, amount=100):
        if account is None:
            account = AccountFactory()

        return TransactionFactory(account=account, amount=amount)
