# Transaction Management Fullstack

Prerequisites
- Installed Docker 


To start service only run ./run from the root folder 
```bash
./run
```

Make sure Node outputs that its ready, then 

- npx cypress open

or 
- npx cypress run 


Endpoints
```html
React http://localhost:3000
Backend http://localhost:8001
```

If something with main run file goes wrong, can try run separate run files from *be* and *fe* folders


### Task

Your task is to **build a fullstack app** that **fulfills the [Transaction Management]** and **make the provided E2E tests pass**.

Backend to Use Python/.NET Core and Frontend React/Angular

Here's how the frontend could look:

![Mockup](https://user-images.githubusercontent.com/1162212/116609549-cbf29b80-a934-11eb-876e-6d5c20061f13.png)

Feel free to tweak the UI, but please ensure that the following HTML is in place.

#### The form for submitting transactions

```html
<form ...>
    <input data-type="account-id" ... />
    <input data-type="amount" ... />
    <input data-type="transaction-submit" type="submit" ... />
</form>
```

Both input **fields should be cleared** after the form is submitted.

#### The transaction list

Every new transaction goes on **the top of the list** and should have an enclosing `<div />` with the following structure:

```html
<div
    data-type="transaction"
    data-account-id="${transaction-account-id}"
    data-amount="${transaction-amount}"
    data-balance="${current-account-balance}"
    ...
>
    ...
</div>
```

-   `${transaction-account-id}` - account id of the corresponding transaction.
-   `${transaction-amount}` - transaction amount.
-   `${current-account-balance}` - the current account balance right after submitting the transaction (only needed to be initialized for the transactions submitted from the current client).


